package converter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Application {
    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.out.println("Pleas enter filename.");
            return;
        }

        Segmentation segmentation =  new Segmentation();

        Path path = Paths.get(args[0]);
        byte[] bytes = Files.readAllBytes(path);
        String text = new String(bytes);

        System.out.println(segmentation.convert(text));
    }
}
