package converter;


import org.apache.commons.lang.StringUtils;

public class Segmentation extends Converter {
    @Override
    public String getName() {
        return "Segmentation";
    }

    private RegEx separators = RegEx.create("seg_sep.txt");
    private RegEx exceptions = RegEx.create("seg_exc.txt");

    @Override
    public String convert(String text) {
        Pair exceptions = getPair(this.exceptions, text, s -> getUnique());
        Pair separators = getPair(this.separators, text, s -> getUnique());

        String[] uniqueWithSep = new String[separators.second.length];

        for (int i = 0; i < separators.second.length; i++) {
            uniqueWithSep[i] =  separators.second[i] + '\n';
        }
        text = StringUtils.replaceEach(text, exceptions.first, exceptions.second);
        text = StringUtils.replaceEach(text, separators.first, uniqueWithSep);
        text = StringUtils.replaceEach(text, separators.second, separators.first);
        text = StringUtils.replaceEach(text, exceptions.second, exceptions.first);
        text = text.trim().replaceAll(" +", " ").replaceAll("\n ", "\n");
        return text;
    }

}